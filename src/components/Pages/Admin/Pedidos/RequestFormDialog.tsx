import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import LinearProgress from '@material-ui/core/LinearProgress';
import Slide from '@material-ui/core/Slide';
import makeStyles from '@material-ui/core/styles/makeStyles';
// import Typography from '@material-ui/core/Typography';
// import FieldCheckbox from '@react-form-fields/material-ui/components/Checkbox';
// import CustomMessage from '@react-form-fields/material-ui/components/CustomMessage';
import FormValidation from '@react-form-fields/material-ui/components/FormValidation';
// import FieldHidden from '@react-form-fields/material-ui/components/Hidden';
import FieldText from '@react-form-fields/material-ui/components/Text';
// import ErrorMessage from 'components/Shared/ErrorMessage';
import Toast from 'components/Shared/Toast';
import { logError } from 'helpers/rxjs-operators/logError';
import useModel from 'hooks/useModel';
import IRequest from 'interfaces/models/request';
// import IRequestRole from 'interfaces/models/RequestRole';
import React, { forwardRef, Fragment, memo, useCallback, useState } from 'react';
import { useCallbackObservable } from 'react-use-observable';
import { of } from 'rxjs';
import { filter, switchMap, tap } from 'rxjs/operators';
import requestService from 'services/request';

interface IProps {
  opened: boolean;
  request?: IRequest;
  onComplete: (request: IRequest) => void;
  onCancel: () => void;
}

const useStyle = makeStyles({
  content: {
    width: 400,
    maxWidth: 'calc(95vw - 50px)'
  },
  heading: {
    marginTop: 20,
    marginBottom: 10
  }
});

const UserFormDialog = memo((props: IProps) => {
  const classes = useStyle(props);

  const [model, setModelProp, setModel, , clearModel] = useModel<IRequest>({});
  const [loading, setLoading] = useState<boolean>(false);

  // const [roles, rolesError, , retryRoles] = useRetryableObservable<Array<IUserRole>>(() => {
  //   setLoading(true);

  //   return userService.roles().pipe(
  //     tap(
  //       () => setLoading(false),
  //       () => setLoading(false)
  //     ),
  //     logError()
  //   );
  // }, []);

  const handleEnter = useCallback(() => {
    setModel({ ...(props.request || {}) });
  }, [props.request, setModel]);

  const handleExit = useCallback(() => {
    clearModel();
  }, [clearModel]);

  const [onSubmit] = useCallbackObservable(
    (isValid: boolean) => {
      return of(isValid).pipe(
        filter(isValid => isValid),
        tap(() => setLoading(true)),
        switchMap(() => requestService.save(model as IRequest)),
        tap(
          request => {
            Toast.show(`O Pedido foi salvo`);
            props.onComplete(request);
            setLoading(false);
          },
          err => {
            Toast.error(err.message === 'email-unavailable' ? 'Email já utlizado' : err);
            setLoading(false);
          }
        ),
        logError()
      );
    },
    [model]
  );

  return (
    <Dialog
      open={props.opened}
      disableBackdropClick
      disableEscapeKeyDown
      onEnter={handleEnter}
      onExited={handleExit}
      TransitionComponent={Transition}
    >
      {loading && <LinearProgress color='secondary' />}

      <FormValidation onSubmit={onSubmit}>
        <DialogTitle>{model.id ? 'Editar' : 'Novo'} Usuário</DialogTitle>
        <DialogContent className={classes.content}>
            <Fragment>
              <FieldText
                label='Descrição'
                disabled={loading}
                value={model.description}
                validation='required|min:5|max:250'
                onChange={setModelProp('description', (model, v) => (model.description = v))}
              />

              <FieldText
                type='number'
                label='Preço'
                disabled={loading}
                value={model.price}
                validation='required|numeric'
                onChange={setModelProp('price', (model, v) => (model.price = v))}
              />

              <FieldText
                type='number'
                label='Quantidade'
                disabled={loading}
                value={model.quantity}
                validation='required|numeric'
                onChange={setModelProp('quantity', (model, v) => (model.quantity = v))}
              />
              {/* <Typography variant='subtitle1' className={classes.heading}>
                Acesso
              </Typography> */}
            </Fragment>
        </DialogContent>
        <DialogActions>
          <Button onClick={props.onCancel}>Cancelar</Button>
          <Button color='primary' type='submit' disabled={loading}>
            Salvar
          </Button>
        </DialogActions>
      </FormValidation>
    </Dialog>
  );
});

const Transition = memo(
  forwardRef((props: any, ref: any) => {
    return <Slide direction='up' {...props} ref={ref} />;
  })
);

export default UserFormDialog;
